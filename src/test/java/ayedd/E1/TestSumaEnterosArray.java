package ayedd.E1;


import java.util.Arrays;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

import java.io.File;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.contrib.java.lang.system.ExpectedSystemExit ;
import org.junit.Rule;


public class TestSumaEnterosArray {
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    public void ioTest(String comentario, String esperado, String[] args) {
	System.out.println("\n"+ comentario + "\nEsperado:");
	System.out.print(esperado);
	System.out.print("----");
	System.out.println("\nEncontrado:");
	SumaEnterosArray.main(args);
	System.out.println("----");
    }

    
    @Test
    public void SumandosDelMismoTamanoSinAcarreo() {
	int a[] = {8,8,8};
	int b[] = {1,0,0};
        int r[] = {9,8,8};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[9, 8, 8]\n";
    	String[] args = {"E1/888.txt","E1/100.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }


    @Test
    public void SumandosDelMismoTamanoConAcarreo() {
	int a[] =   {8,8,8};
	int b[] =   {8,8,8};
        int r[] = {1,7,7,6};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[1, 7, 7, 6]\n";
	String[] args = {"E1/888.txt","E1/888.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }


    @Test
    public void SumandosDeDistintoTamanoSinAcarreo() {
	int a[] =   {1,0,0};
	int b[] = {1,8,8,8};
        int r[] = {1,9,8,8};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[1, 9, 8, 8]\n";
	String[] args = {"E1/100.txt","E1/1888.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }

    @Test
    public void SumandosDeDistintoTamanoSinAcarreo2() {
	int a[] = {1,8,8,8};
	int b[] =   {1,0,0};
        int r[] = {1,9,8,8};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[1, 9, 8, 8]\n";
    	String[] args = {"E1/1888.txt","E1/100.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }

    @Test
    public void SumandosDeDistintoTamanoConAcarreo() {
	int a[] =   {8,8,8};
	int b[] = {1,8,8,8};
        int r[] = {2,7,7,6};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[2, 7, 7, 6]\n";
    	String[] args = {"E1/888.txt","E1/1888.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }

    @Test
    public void SumandosDeDistintoTamanoConAcarreo2() {
	int a[] = {1,8,8,8};
	int b[] =   {8,8,8};
        int r[] = {2,7,7,6};
	String comentario = Arrays.toString(a) + " + " + Arrays.toString(b);

	String esperado = "[2, 7, 7, 6]\n";
    	String[] args = {"E1/1888.txt","E1/888.txt"};
	ioTest(comentario, esperado, args);

	assertThat(SumaEnterosArray.suma(a,b)).as(comentario).containsExactly(r);
    }
}
