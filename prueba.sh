#!/usr/bin/bash

if [[ $# -ne 2 ]];
then
    echo "Tienes que introducir los tests y un comentario:"
    echo "    ./prueba.sh \"Test\" \"Comentario\" ";
    echo "                 Test es el nombre de los tests que quieres probar"
    echo "Ejemplo:"
    echo "    ./prueba.sh \"ayedd.E1.TestSumaEnterosArray.java\" \"Arreglado código para acarreo\" "
    exit 1
fi

{
    git checkout rama-alumno

    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno


    git add 'target/surefire-reports/*.txt'
    git add '*.java'
    git add '*LEEME'

    git commit -am "$2"
} > ./prueba.output 2>&1


mvn -Dmaven.test.failure.ignore=true -Dtest="$1" test | egrep -v "WARNING"
mvn  exec:exec -Dexec.args="target/surefire-reports/$1-output.txt" | egrep -v "WARNING"

