#!/usr/bin/bash

if [[ $# -ne 1 ]];
then
    echo "Tienes que introducir un comentario:"
    echo "    ./entrega.sh \"Comentario que explique los cambios introducidos en esta versión que vas a entregar\" ";
    exit 1
fi

{
    git checkout rama-alumno

    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno

    git merge master

    git add 'target/surefire-reports/*.txt'
    git add '*LEEME'

    git commit -am "$1"
    git push origin rama-alumno
} > ./entrega.output 2>&1

